import App from '../../app';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  // call showModal function

  showModal({
    title: `${fighter.name} winner!`,
    bodyElement: 'Congratulation!',
    onClose() {
      root.innerHTML = '';
      new App();
    }
  });
}
