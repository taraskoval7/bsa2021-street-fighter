import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {

  const positionClassName = (position === 'right') ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({tagName: 'div', className: `fighter-preview___root ${positionClassName}`,});

  // todo: show fighter info (image, name, health, etc.)

  if (!fighter) return '';

  const imageElement = createFighterImage(fighter);

  const nameElement = createElement({tagName: 'div', className: 'fighter-preview___name'});
  nameElement.innerHTML = fighter.name;

  const infoElement = createElement({tagName: 'div', className: 'fighter-preview___info'});
  infoElement.innerHTML = `health: ${fighter.health} <br> attack: ${fighter.attack} <br> defence: ${fighter.defense}`;

  fighterElement.append(imageElement, nameElement, infoElement);

  return fighterElement;
}

export function createFighterImage(fighter) {

  const { source, name } = fighter;

  const attributes = {
    src: source,
    title: name,
    alt: name
  };

  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
