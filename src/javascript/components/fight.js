import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const leftHealthBar = document.getElementById('left-fighter-indicator');
    const rightHealthBar = document.getElementById('right-fighter-indicator');

    const leftHealthMax = firstFighter.health;
    const rightHealthMax = secondFighter.health;

    let firstInBlockFlag = false;
    let secondInBlockFlag = false;

    let firstFighterCooldown = Date.now() - 10000;
    let secondFighterCooldown = Date.now() - 10000;


    document.addEventListener('keydown', (event) => {
      switch (event.code) {
        case controls.PlayerOneAttack:
          console.log('First fighter attack');

          if (!firstInBlockFlag) {
            if (!secondInBlockFlag) {
              secondFighter.health -= getDamage(getHitPower(firstFighter), getBlockPower(secondFighter));
            } else {
              secondFighter.health -= 0;
            }
          } else {
            console.log('First fighter cant attack in the block');
          }

          if (secondFighter.health < rightHealthMax) {
            rightHealthBar.style.width = (secondFighter.health <= 0)? '0%' :
              100 / (rightHealthMax / secondFighter.health) + '%';
          }

          if (secondFighter.health <= 0)
            resolve(firstFighter);

          break;
        case controls.PlayerOneBlock:

          firstInBlockFlag = true;
          console.log("First fighter in block");

          break;
        case controls.PlayerTwoAttack:
          console.log('Second fighter attack');

          if (!secondInBlockFlag) {
            if (!firstInBlockFlag) {
              firstFighter.health -= getDamage(getHitPower(secondFighter), getBlockPower(firstFighter));
            } else {
              firstFighter.health -= 0;
            }
          } else {
            console.log('Second fighter cant attack in the block');
          }

          if (firstFighter.health < leftHealthMax) {
            leftHealthBar.style.width = (firstFighter.health <= 0)? '0%' :
              100 / (leftHealthMax / firstFighter.health) + '%';
          }

          if (firstFighter.health <= 0)
            resolve(secondFighter);

          break;
        case controls.PlayerTwoBlock:

          secondInBlockFlag = true;
          console.log("Second fighter in block");

          break;
      }
    });


    document.addEventListener('keyup', (event) => {
      switch(event.code) {
        case controls.PlayerOneBlock:
          if (firstInBlockFlag) {
            firstInBlockFlag = false;
            console.log("First fighter left the block");
          }
          break;
        case controls.PlayerTwoBlock:
          if (secondInBlockFlag) {
            secondInBlockFlag = false;
            console.log("Second fighter left the block");
          }
          break;
      }
    });

    runOnKeys(() => {

        if (Date.now() > firstFighterCooldown + 10000){
          console.log('First fighter critical attack');

          secondFighter.health -= getCriticalDamage(firstFighter)
          firstFighterCooldown = Date.now();
        } else {
          console.log('First fighter must wait for critical attack - ' +
            (firstFighterCooldown + 10000 - Date.now()) / 1000);
        }

        if (secondFighter.health < rightHealthMax) {
          rightHealthBar.style.width = (secondFighter.health <= 0)? '0%' :
            100 / (rightHealthMax / secondFighter.health) + '%';
        }

        if (secondFighter.health <= 0)
          resolve(firstFighter);
      },
      ...controls.PlayerOneCriticalHitCombination
    );

    runOnKeys(() => {

        if (Date.now() > secondFighterCooldown + 10000){
          console.log('Second fighter critical attack');

          firstFighter.health -= getCriticalDamage(secondFighter)
          secondFighterCooldown = Date.now();
        } else {
          console.log('Second fighter must wait for critical attack - ' +
            (secondFighterCooldown + 10000 - Date.now()) / 1000);
        }

        if (firstFighter.health < leftHealthMax) {
          leftHealthBar.style.width = (firstFighter.health <= 0)? '0%' :
            100 / (leftHealthMax / firstFighter.health) + '%';
        }

        if (firstFighter.health <= 0)
          resolve(secondFighter);
      },
      ...controls.PlayerTwoCriticalHitCombination
    );

  });
}

export function runOnKeys(func, ...codes) {
  let pressed = new Set();

  document.addEventListener('keydown', function(event) {
    pressed.add(event.code);

    for (let code of codes)
      if (!pressed.has(code))
        return;

    pressed.clear();

    func();
  });

  document.addEventListener('keyup', function(event) {
    pressed.delete(event.code);
  });
}

export function getCriticalDamage(fighter) {
  return fighter.attack * 2;
}

export function getDamage(attacker, defender) {
  // return damage
  return (attacker - defender > 0) ? attacker - defender : 0;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = (1 + Math.random() * (2 - 1));
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = (1 + Math.random() * (2 - 1));
  return fighter.defense * dodgeChance;
}
